import Layout from '@/components/layout';
import React from 'react';
import PostData from '@/lib/post-data';
import { GetStaticPaths, GetStaticProps } from 'next';
import { getAllPostIDs, getPostData } from '@/lib/posts';
import { useRouter } from 'next/router';
import Head from 'next/head';
import DefaultErrorPage from 'next/error';

interface PostProps {
    postData?: PostData;
}

const Post: React.FC<PostProps> = ({ postData }) => {
    const router = useRouter();
    if (router.isFallback) {
        return <Layout>Loading ...</Layout>;
    }
    if (!postData) {
        return <DefaultErrorPage statusCode={404} />;
    }

    return (
        <Layout>
            <Head>
                <title>{postData.title}</title>
            </Head>
            <br />
            {postData.id}
            <br />
            {postData.date}
            <br />
            <div
                dangerouslySetInnerHTML={{
                    __html: postData.contentHtml as string,
                }}
            />
        </Layout>
    );
};

export const getStaticPaths: GetStaticPaths = async () => {
    const ids = getAllPostIDs();
    const paths = ids.map((id) => ({
        params: {
            id,
        },
    }));
    return {
        paths,
        fallback: true,
    };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
    const postData = getPostData(params?.id as string, true);
    return {
        props: {
            postData,
        },
        revalidate: 1,
    };
};

// export const getServerSideProps: GetStaticProps = async ({ params }) => {
//     const postData = getPostData(params?.id as string, true);
//     return {
//         props: {
//             postData,
//         },
//     };
// };

export default Post;
