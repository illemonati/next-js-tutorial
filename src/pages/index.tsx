import Head from 'next/head';
import React from 'react';
import Layout, { siteTitle } from '@/components/layout';
import utilStyles from '@/styles/utils.module.css';
import { getSortedPostDatas } from '@/lib/posts';
import PostData from '@/lib/post-data';
import Link from 'next/link';
import { GetStaticProps, GetServerSideProps } from 'next';

interface HomeProps {
    sortedPostDatas: PostData[];
}

const Home: React.FC<HomeProps> = ({ sortedPostDatas }) => (
    <Layout home>
        <Head>
            <title>{siteTitle}</title>
        </Head>
        <section className={utilStyles.headingMd}>
            <p>[Your Self Introduction]</p>
            <p>
                (This is a sample website - you’ll be building a site like this
                on{` `}
                <a href="https://nextjs.org/learn">our Next.js tutorial</a>
                .)
            </p>
        </section>
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
            <h2 className={utilStyles.headingLg}>Blog</h2>
            <ul className={utilStyles.list}>
                {sortedPostDatas.map(({ id, date, title }) => (
                    <li className={utilStyles.listItem} key={id}>
                        <Link href={`/post/${id}`}>
                            <a>{title}</a>
                        </Link>
                        <br />
                        {id}
                        <br />
                        {date}
                    </li>
                ))}
            </ul>
        </section>
    </Layout>
);

// getServerSideProps for real time
// getStaticProps for static

export const getStaticProps: GetStaticProps = async () => {
    const sortedPostDatas = getSortedPostDatas();
    return {
        props: {
            sortedPostDatas,
        },
        revalidate: 1,
    };
};

// export const getServerSideProps: GetServerSideProps = async () => {
//     const sortedPostDatas = getSortedPostDatas();
//     return {
//         props: {
//             sortedPostDatas,
//         },
//     };
// };

export default Home;
