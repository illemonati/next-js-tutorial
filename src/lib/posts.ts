import fs from 'fs';
import path from 'path';
import matter from 'gray-matter';
import PostData from '@/lib/post-data';
import marked from 'marked';

const postsDirectory = path.join(process.cwd(), `posts`);

export const getAllPostIDs = () => {
    const filenames = fs.readdirSync(postsDirectory);
    return filenames.map((filename) => filename.replace(/\.md$/, ``));
};

export const getPostData = (
    id: string,
    processContent?: boolean,
): PostData | null => {
    try {
        const fullPath = path.join(postsDirectory, `${id}.md`);
        const fileContent = fs.readFileSync(fullPath, `utf8`);

        const matterResult = matter(fileContent);

        const contentHtml = processContent
            ? marked(matterResult.content)
            : null;

        return {
            id,
            contentHtml,
            ...matterResult.data,
        };
    } catch (e) {
        return null;
    }
};

export const getSortedPostDatas = () => {
    const ids = getAllPostIDs();
    const allPosts: PostData[] = ids.map((id) => getPostData(id)!);
    return allPosts.sort((a, b) => b.date - a.date);
};
