export default interface PostData {
    id: string;
    contentHtml?: string | null;
    [key: string]: any;
}
